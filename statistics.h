#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#ifndef STATISTICS_H_
#define STATISTICS_H_

typedef struct
{
  //the tests real-times
  double * real_times;

  //the tests user-times
  double * user_times;

  //the current test_counter
  int test_counter;

  //total number of tests
  int num_tests;

  //number of workers
  int num_workers;

  //upper and lower percentage of results to be discarded.
  double filter;

} statistics_t;

statistics_t *
create_statistics(int nworkers, int ntests);

void
delete_statistics(statistics_t *);

void
set_statistics(int rt_avg, int ut_avg, int rt_v, int ut_v, int num_cpus,
    int cpu_occupation);

void
show_statistics(statistics_t *, statistics_t *);

void
save_statistics(statistics_t *, statistics_t *, char* filename);

void
set_filter(statistics_t *, double filter);

double
get_real_time_average(statistics_t *);

double
get_real_time_variance(statistics_t *);

double
get_user_time_average(statistics_t *);

double
get_user_time_variance(statistics_t *);

void
add_time(statistics_t*, double, double);

double
get_cpu_occupation(statistics_t*);

#endif
