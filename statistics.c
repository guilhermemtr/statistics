#include "statistics.h"
#ifdef STATISTICS_H_

//the statistical tests to be made.
int rt_avg = 1;
char * rt_avg_name = "Realtime average";
int ut_avg = 1;
char * ut_avg_name = "Usertime average";
int rt_v = 1;
char * rt_v_name = "Realtime variance";
int ut_v = 1;
char * ut_v_name = "Usertime variance";
int num_cpus = 1;
char * num_cpus_name = "#Cpus";
int cpu_occupation = 1;
char * cpu_occupation_name = "Cpu occupation";
int speed_up = 1;
char * speed_up_name = "Speedup";

statistics_t *
create_statistics(int nworkers, int ntests)
{
  statistics_t * new_t = (statistics_t *) malloc(sizeof(statistics_t));
  new_t->real_times = (double *) malloc(ntests * sizeof(double));
  new_t->user_times = (double *) malloc(ntests * sizeof(double));
  new_t->test_counter = 0;
  new_t->num_tests = ntests;
  new_t->num_workers = nworkers;
  new_t->filter = 0.0;
  return new_t;
}

void
delete_statistics(statistics_t * s)
{
  free(s->real_times);
  free(s->user_times);
  free(s);
}

void
set_statistics(int rt_avg_, int ut_avg_, int rt_v_, int ut_v_, int num_cpus_,
    int cpu_occupation_)
{
  rt_avg = rt_avg_;
  ut_avg = ut_avg_;
  rt_v = rt_v_;
  ut_v = ut_v_;
  num_cpus = num_cpus_;
  cpu_occupation = cpu_occupation_;
}

double
get_speedup(statistics_t* s_n, statistics_t * s_1)
{
  return get_real_time_average(s_1) / get_real_time_average(s_n);
}

void
show_statistics(statistics_t * s, statistics_t * s_1)
{
  if (!(s->test_counter == s->num_tests && s->num_tests))
    return;
  if (num_cpus)
    {
      printf("%s\t%d\n", num_cpus_name, s->num_workers);
    }
  if (rt_avg)
    {
      printf("%s\t%lf\n", rt_avg_name, get_real_time_average(s));
    }
  if (rt_v)
    {
      printf("%s\t%lf\n", rt_v_name, get_real_time_variance(s));
    }
  if (ut_avg)
    {
      printf("%s\t%lf\n", ut_avg_name, get_user_time_average(s));
    }
  if (ut_v)
    {
      printf("%s\t%lf\n", ut_v_name, get_user_time_variance(s));
    }
  if (cpu_occupation)
    {
      printf("%s\t%lf\n", cpu_occupation_name, get_cpu_occupation(s));
    }
  if (speed_up)
    {
      printf("%s\t%lf\n", speed_up_name, get_speedup(s, s_1));
    }
  printf("\n\n\n");
}

void
save_statistics(statistics_t * s, statistics_t * s_1, char* filename)
{
  if (!(s->test_counter == s->num_tests && s->num_tests))
    return;
  FILE * f = fopen(filename, "rb");
  int exists = f != NULL;
  if (exists)
    fclose(f);
  f = fopen(filename, "a+");
  char * separator = " , ";
  if (!exists)
    {
      fprintf(f, "%s%s", num_cpus_name, separator);
      fprintf(f, "%s%s", rt_avg_name, separator);
      fprintf(f, "%s%s", rt_v_name, separator);
      fprintf(f, "%s%s", ut_avg_name, separator);
      fprintf(f, "%s%s", ut_v_name, separator);
      fprintf(f, "%s%s", cpu_occupation_name, separator);
      fprintf(f, "%s\n", speed_up_name);
    }

  if (num_cpus)
    {
      fprintf(f, "%d%s", s->num_workers, separator);
    }
  if (rt_avg)
    {
      fprintf(f, "%lf%s", get_real_time_average(s), separator);
    }
  if (rt_v)
    {
      fprintf(f, "%lf%s", get_real_time_variance(s), separator);
    }
  if (ut_avg)
    {
      fprintf(f, "%lf%s", get_user_time_average(s), separator);
    }
  if (ut_v)
    {
      fprintf(f, "%lf%s", get_user_time_variance(s), separator);
    }
  if (cpu_occupation)
    {
      fprintf(f, "%lf%s", get_cpu_occupation(s), separator);
    }
  if (speed_up)
    {
      fprintf(f, "%lf", get_speedup(s, s_1));
    }
  fprintf(f, "\n", get_cpu_occupation(s));
  fclose(f);
}

void
set_filter(statistics_t *s, double filter)
{
  s->filter = filter;
}

int
compare(const void * a, const void * b)
{
  double x1 = *(double *) a;
  double x2 = *(double *) b;
  if (x1 == x2)
    return 0;
  if (x1 < x2)
    return -1;
  else
    return 1;
}

double
get_average(double * results, int size, double filter)
{
  double average = 0.0;
  qsort(results, size, sizeof(double), compare);
  int base = size * filter;
  int limit = size - size * filter;
  int i = base;
  for (; i < limit; i++)
    {
      average = average + results[i] / ((double) limit - base);
    }
  return average;
}

double
get_variance(double * results, int size, double average, double filter)
{
  qsort(results, size, sizeof(double), compare);
  int base = size * filter;
  int limit = size - size * filter;
  double variance = 0.0;
  int i = base;
  for (; i < limit; i++)
    {
      variance = variance + pow(results[i] - average, 2.0);
    }
  variance = variance / ((double) (size - filter * size * 2.0) - 1.0);
  return variance;
}

double
get_real_time_average(statistics_t *s)
{
  if (!(s->test_counter == s->num_tests && s->num_tests))
    return -1.0;
  return get_average(s->real_times, s->num_tests, s->filter);
}

double
get_real_time_variance(statistics_t *s)
{
  if (!(s->test_counter == s->num_tests && s->num_tests))
    return -1.0;
  return get_variance(s->real_times, s->num_tests, get_real_time_average(s),
      s->filter);
}

double
get_user_time_average(statistics_t *s)
{
  if (!(s->test_counter == s->num_tests && s->num_tests))
    return -1.0;
  return get_average(s->user_times, s->num_tests, s->filter);
}

double
get_user_time_variance(statistics_t *s)
{
  if (!(s->test_counter == s->num_tests && s->num_tests))
    return -1.0;
  return get_variance(s->user_times, s->num_tests, get_user_time_average(s),
      s->filter);
}

void
add_time(statistics_t *s, double real_time, double user_time)
{
  if (!(s->test_counter < s->num_tests && s->num_tests))
    return;
  s->real_times[s->test_counter] = real_time;
  s->user_times[s->test_counter] = user_time;
  s->test_counter++;
}

double
get_cpu_occupation(statistics_t *s)
{
  if (!(s->test_counter == s->num_tests && s->num_tests))
    return -1.0;
  return get_user_time_average(s) / get_real_time_average(s);
}

#endif
